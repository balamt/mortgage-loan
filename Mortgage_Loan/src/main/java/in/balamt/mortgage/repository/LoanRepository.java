package in.balamt.mortgage.repository;

import in.balamt.mortgage.exceptions.LoanNotFoundException;
import in.balamt.mortgage.model.Customer;
import in.balamt.mortgage.model.Loan;
import in.balamt.mortgage.model.LoanStatus;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class LoanRepository {

    public List<Loan> getAll() {
        List<Loan> loans = new ArrayList<>();

        Loan l1 = new Loan();
        l1.setLoanId(1200L);
        l1.setLoanAmount(44000d);
        l1.setLoanStatus(LoanStatus.NEW);
        Customer c1 = new Customer();
        c1.setCustomerId(900L);
        c1.setName("Hari");
        l1.setCustomer(c1);

        Loan l2 = new Loan();
        l2.setLoanId(1201L);
        l2.setLoanAmount(23444d);
        l2.setLoanStatus(LoanStatus.PROCESSED);
        Customer c2 = new Customer();
        c2.setCustomerId(901L);
        c2.setName("Lalitha");
        l2.setCustomer(c2);

        loans.add(l1);
        loans.add(l2);
        return loans;
    }

    public Loan findLoanById(Long loanId) throws LoanNotFoundException {
        if(loanId < 2000){
            throw new LoanNotFoundException("Unable to find the loan");
        }

        Loan l2 = new Loan();
        l2.setLoanId(1201L);
        l2.setLoanAmount(23444d);
        l2.setLoanStatus(LoanStatus.PROCESSED);
        Customer c2 = new Customer();
        c2.setCustomerId(901L);
        c2.setName("Lalitha");
        l2.setCustomer(c2);

        return l2;

    }
}
