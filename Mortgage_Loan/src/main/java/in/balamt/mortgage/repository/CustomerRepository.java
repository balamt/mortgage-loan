package in.balamt.mortgage.repository;

import in.balamt.mortgage.exceptions.CustomerNotFoundException;
import in.balamt.mortgage.model.Customer;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerRepository {
    public Customer getCustomerById(Long customerId) throws CustomerNotFoundException {
        if(customerId <= 0){
            throw new CustomerNotFoundException("No Such Customer record matched");
        }
        Customer customer = new Customer();
        customer.setName("Some");
        customer.setCustomerId(345L);;
        return customer;

    }
}
