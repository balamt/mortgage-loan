package in.balamt.mortgage.controller;

import in.balamt.mortgage.exceptions.LoanNotFoundException;
import in.balamt.mortgage.model.Loan;
import in.balamt.mortgage.service.MortgageLoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/loan/mortgage/")
public class MortgageLoanController {

    @Autowired
    MortgageLoanService mortgageLoanService;

    @GetMapping("/test")
    public ResponseEntity<String> testServer(){
        return ResponseEntity.ok("Mortgage Loan Service Up and Running...");
    }

    @GetMapping("/all")
    public ResponseEntity<List<Loan>> getAllLoans(){
        List<Loan> loans = mortgageLoanService.getAllLoans();
        return ResponseEntity.ok(loans);
    }

    @GetMapping("/{loanId}")
    public ResponseEntity<Loan> getLoanById(@PathVariable Long loanId) throws LoanNotFoundException {
        return ResponseEntity.ok(mortgageLoanService.getLoanById(loanId));
    }

    /***
     *
     * ExceptionHandler can be used in the controller itself.
     * or we can move it to common class where we handle all the exceptions.
     *
     * @return
     */
    /*@ExceptionHandler(LoanNotFoundException.class)
    public ResponseEntity loanNotFound(){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }*/

}
