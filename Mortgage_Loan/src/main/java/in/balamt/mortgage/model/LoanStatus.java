package in.balamt.mortgage.model;

public enum LoanStatus {

    NEW("New"),
    CLOSED("Closed"),
    HOLD("Hold"),
    PROCESSING("Processing"),
    PROCESSED("Processed");

    private String description;

    LoanStatus(String description){
        this.description = description;
    }

    public String description(){
        return this.description;
    }

}
