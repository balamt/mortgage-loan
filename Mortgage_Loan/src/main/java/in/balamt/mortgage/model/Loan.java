package in.balamt.mortgage.model;

import java.io.Serializable;
import java.util.Objects;

public class Loan implements Serializable {

    private Long loanId;
    private Double loanAmount;
    private LoanStatus loanStatus;
    private Customer customer;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loan loan = (Loan) o;
        return Objects.equals(loanId, loan.loanId) &&
                Objects.equals(loanAmount, loan.loanAmount) &&
                Objects.equals(loanStatus, loan.loanStatus) &&
                Objects.equals(customer, loan.customer);
    }

    @Override
    public int hashCode() {

        return Objects.hash(loanId, loanAmount, loanStatus, customer);
    }

    public Long getLoanId() {
        return loanId;
    }

    public void setLoanId(Long loanId) {
        this.loanId = loanId;
    }

    public LoanStatus getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(LoanStatus loanStatus) {
        this.loanStatus = loanStatus;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Double getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Double loanAmount) {
        this.loanAmount = loanAmount;
    }
}
