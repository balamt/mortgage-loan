package in.balamt.mortgage.exceptions;

public class CustomerNotFoundException extends Exception {

    public CustomerNotFoundException(String message){
        super(message);
    }

    public CustomerNotFoundException(String message, Exception exce){
        super(message, exce);
    }

}
