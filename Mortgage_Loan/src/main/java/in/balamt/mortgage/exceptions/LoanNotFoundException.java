package in.balamt.mortgage.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LoanNotFoundException extends Exception {

    public LoanNotFoundException(String message){
        super(message);
    }

    public LoanNotFoundException(String message, Exception exce){
        super(message, exce);
    }
}
