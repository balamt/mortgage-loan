package in.balamt.mortgage.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class MyAppGlobalExceptionHandlers extends ResponseEntityExceptionHandler {

    /**
     * We can also have the ExceptionHandler inside our ControllerAdvice,
     * which is considered as Global Exception Handler.
     *
     * @return
     */

    @ExceptionHandler({CustomerNotFoundException.class})
    public void customerNotFound(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value()," Customer Not Found");
    }

    @ExceptionHandler({LoanNotFoundException.class})
    public void loanNotFound(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value()," Loan Not Found - Treating this as Bad request for example");
    }


}
