package in.balamt.mortgage.service;

import in.balamt.mortgage.exceptions.CustomerNotFoundException;
import in.balamt.mortgage.model.Customer;
import in.balamt.mortgage.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public Customer getCustomerById(Long customerId) throws CustomerNotFoundException {
        return customerRepository.getCustomerById(customerId);
    }
}
