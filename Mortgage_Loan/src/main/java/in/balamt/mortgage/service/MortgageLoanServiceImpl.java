package in.balamt.mortgage.service;

import in.balamt.mortgage.exceptions.LoanNotFoundException;
import in.balamt.mortgage.model.Loan;
import in.balamt.mortgage.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MortgageLoanServiceImpl implements MortgageLoanService{

    @Autowired
    LoanRepository loanRepository;

    @Override
    public List<Loan> getAllLoans() {
        return loanRepository.getAll();
    }

    @Override
    public Loan getLoanById(Long loanId) throws LoanNotFoundException {
        return loanRepository.findLoanById(loanId);
    }
}
