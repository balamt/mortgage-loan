package in.balamt.mortgage.service;

import in.balamt.mortgage.exceptions.CustomerNotFoundException;
import in.balamt.mortgage.model.Customer;

public interface CustomerService {
    Customer getCustomerById(Long customerId) throws CustomerNotFoundException;
}
