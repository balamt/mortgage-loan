package in.balamt.mortgage.service;

import in.balamt.mortgage.exceptions.LoanNotFoundException;
import in.balamt.mortgage.model.Loan;

import java.util.List;

public interface MortgageLoanService {
    public List<Loan> getAllLoans();
    public Loan getLoanById(Long loanId) throws LoanNotFoundException;
}
